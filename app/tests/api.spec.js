const app = require('../app');
const chai = require('chai');
const request = require('supertest');

const expect = chai.expect;

describe('API Test without query params', function() {
    it('GET :: /people-like-you should return 200', function(done) {
        request(app)
            .get('/people-like-you')
            .end(function(err, res) {
                expect(res.statusCode).to.be.equal(200);
                done();
            });
    });
});

describe('API Test with query params', function() {
    it('GET :: /people-like-you should return 200', function(done) {
        request(app)
            .get('/people-like-you?name=raj&age=25&latitude=50.5345697&longitude=19.56667&monthlyIncome=9800&experienced=true')
            .end(function(err, res) {
                expect(res.statusCode).to.be.equal(200);
                done();
            });
    });
});