const bambuServiceClient = require('../model/grpc/index').initBambuMicroService();

const service = {};

service.getPeopleLikeYou = function (payload, cb) {
    // console.log(`payload at service.getPeopleLikeYou : [${payload}]`);
    return bambuServiceClient.getPeopleLikeYou(payload, cb);
};

module.exports = service;