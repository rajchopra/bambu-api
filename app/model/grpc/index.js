const rpcClient = require(`bambu-proto/services/bambuService/clients/nodejs/client`);

module.exports = {
    initBambuMicroService: function() {
        const dbconfig = { host: `localhost`, port : 2211}; // read from config
        const timeoutInMs = 500;
        const bambuServiceClient = new rpcClient(dbconfig, timeoutInMs);
        return bambuServiceClient;
    }
};