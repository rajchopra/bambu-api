const bambuService = require(`../services/people.services`);
const utils = require(`../utils/common`);
const controller = {};
// const util = require(`util`)


const validateRequest = function (req) {
    // check properly if the request is a valid request
    return true;
};

controller.getPeopleLikeYou = function (req, res) {
    if (validateRequest()) {
        // console.log(util.inspect(req, {showHidden: false, depth: null}));
        
        console.log(`Going to make GRPC Request with req.params[${JSON.stringify(req.query)}]`);
        bambuService.getPeopleLikeYou(req.query, (err, data) => {
            if (err || !data) {
                console.log(`Error getting similar people. Err[${err}]. Data[${data}]`);
                return res.status(500).json(utils.createRes(false, {}, `Similar people not found`, `DataException`));
            } else {
                // console.log(`Got monthlyIncome type : ` + (typeof data.peopleLikeYou[0].monthlyIncome));
                res.json(utils.createRes(true, data, null));
            }
        });
    } else {
        return res.status(400).json(utils.createRes(false, {}, `Invalid request query params`, `InvalidParams`));
    }
};

module.exports = controller;
