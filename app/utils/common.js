const errorTypes = {
    InvalidParams       : `INVALID_INPUT`,
    ServerSideProblem   : `SERVER_FAILURE`,
    DataException       : `DATA_UPSTREAM`,
    ForbiddenAccess     : `FORBIDDEN`,
    NoAccess            : `UPRADE_REQUIRED`
}

const utils = {};

utils.createRes = function (success, data, error, type) {
    return {
        success: success,
        data: data,
        error: error,
        errorType: error? (errorTypes[type] || `NOT_DEFINED`) : undefined
    };
}

module.exports = utils;